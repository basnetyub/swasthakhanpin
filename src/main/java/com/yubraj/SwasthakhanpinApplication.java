package com.yubraj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwasthakhanpinApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwasthakhanpinApplication.class, args);
	}
}
