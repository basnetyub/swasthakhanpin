package com.yubraj.fooddatas;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by yubraj on 6/17/17.
 */
public interface FoodRepository extends PagingAndSortingRepository<Food,Long> {

    @RestResource(rel="title-contains", path="containsTitle")
    Page<Food> findByTitleContaining(@Param("title")String title, Pageable page);
}
