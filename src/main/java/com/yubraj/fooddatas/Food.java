package com.yubraj.fooddatas;

import com.yubraj.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by yubraj on 6/17/17.
 */
@Table(name = "food")
@Entity
public class Food extends BaseEntity {
    @NotNull
    @Size(min = 3)
    private String title;
    private String subtitle;
    private String calories;
    private String carbs;
    private String fats;
    private String protien;
    private String fiber;
    private String moreinfourl;
    private String imageurl;

    public Food() {
        super();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getCarbs() {
        return carbs;
    }

    public void setCarbs(String carbs) {
        this.carbs = carbs;
    }

    public String getFats() {
        return fats;
    }

    public void setFats(String fats) {
        this.fats = fats;
    }

    public String getProtien() {
        return protien;
    }

    public void setProtien(String protien) {
        this.protien = protien;
    }

    public String getFiber() {
        return fiber;
    }

    public void setFiber(String fiber) {
        this.fiber = fiber;
    }

    public String getMoreinfourl() {
        return moreinfourl;
    }

    public void setMoreinfourl(String moreinfourl) {
        this.moreinfourl = moreinfourl;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
