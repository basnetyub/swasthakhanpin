package com.yubraj.validator;

import com.yubraj.DTO.Userdto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by yubraj on 6/22/17.
 */
public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        Userdto user = (Userdto) obj;
        return user.getPassword().equals(user.getMatchingpassword());
    }
}