package com.yubraj.DTO;

import com.yubraj.validator.PasswordMatches;
import com.yubraj.validator.ValidEmail;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotNull;
import java.sql.Date;

/**
 * Created by yubraj on 6/22/17.
 */
@PasswordMatches
public class Userdto {
    public static final PasswordEncoder PASSWORD_ENCODER=new BCryptPasswordEncoder();
    @NotNull
    @NotEmpty
    private String firstname;
    private String lastname;
    @NotNull
    @NotEmpty
    @Email
    private String email;
    @NotNull
    @NotEmpty
    private String username;
    @NotNull
    @NotEmpty
    private String password;
    @NotNull
    @NotEmpty

    private String matchingpassword;
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingpassword() {
        return matchingpassword;
    }

    public void setMatchingpassword(String matchingpassword) {
        this.matchingpassword = matchingpassword;
    }
}
