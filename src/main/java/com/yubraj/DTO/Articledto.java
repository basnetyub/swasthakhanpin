package com.yubraj.DTO;


import com.yubraj.user.User;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.sql.Date;

/**
 * Created by black_flame on 7/26/2017.
 */
public class Articledto {
    @NotNull
    @NotEmpty
    private User user;

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    @NotEmpty
    private java.sql.Date publishDate;

    @NotNull
    @NotEmpty
    private String description;

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public String getDescription() {
        return description;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
