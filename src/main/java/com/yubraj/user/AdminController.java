package com.yubraj.user;

import com.yubraj.DTO.Articledto;
import com.yubraj.article.Article;
import com.yubraj.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("admin")
public class AdminController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("index")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }
    @GetMapping("searchfoods")
    public ModelAndView searchfoods(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("searchfoods");
        return mav;
    }

    @GetMapping("viewarticle")
    public ModelAndView viewarticle(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("viewarticle");
        return mav;
    }

    @GetMapping("submitarticle")
    public ModelAndView submitarticle(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("submitarticle");
        Articledto article=new Articledto();
        mav.addObject("article",article);
        return mav;
    }

    @PostMapping("submitarticle")
    public ModelAndView storearticle(@ModelAttribute("article")  Articledto articledto){
        System.out.println("hello");
        ModelAndView mav=new ModelAndView();
        Article article=createArticle(articledto);
        mav.setViewName("success");

        return mav;
    }

    private Article createArticle(Articledto articledto) {
        Article article=null;
        try{
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            articledto.setPublishDate(sqlDate);
            String user=SecurityContextHolder.getContext().getAuthentication().getName().toString();
            System.out.println(user);
            User userobj=userRepository.findByUsername(user);
            System.out.println(userobj.toString());
            articledto.setUser(userobj);

            article=new Article(articledto.getTitle(),
                    articledto.getDescription(),
                    articledto.getPublishDate(),
                    articledto.getUser());

            articleRepository.save(article);

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return article;
    }

    @GetMapping("contact")
    public ModelAndView contact(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("contact");
        return mav;
    }

    @GetMapping("editprofile")
    public ModelAndView editprofile(){
        ModelAndView mav=new ModelAndView();
        mav.setViewName("editprofile");
        return mav;
    }

}
