package com.yubraj.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Created by yubraj on 6/13/17.
 */
@Service
public class DetailService implements UserDetailsService {
    @Autowired
    UserRepository users;
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user=users.findByUsername(username);
        if(user==null)
            throw new UsernameNotFoundException(username+"not found");

        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_"+user.getRoles());
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), Arrays.asList(authority));
        return userDetails;
    }

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user=users.findByUsername(username);
//        if(user==null)
//            throw new UsernameNotFoundException(username+"not found");
//
//        return new org.springframework.security.core.userdetails.User(
//                user.getUsername(),
//                user.getPassword(),
//                AuthorityUtils.createAuthorityList(user.getRoles())
//
//        );
//    }
}
