package com.yubraj.user;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by yubraj on 6/17/17.
 */
public interface UserRepository extends CrudRepository<User,Long> {
    User findByUsername(String username);
    User findByEmail(String email);

}
