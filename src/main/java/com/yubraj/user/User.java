package com.yubraj.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yubraj.core.BaseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by yubraj on 6/17/17.
 */
@Entity
public class User extends BaseEntity{
    public static final PasswordEncoder PASSWORD_ENCODER=new BCryptPasswordEncoder();
    private String firstname;
    private String lastname;
    private String email;
    private String username;
    @JsonIgnore
    private String password;
    private java.sql.Date dob;
    //@Temporal(TemporalType.DATE) private java.util.Date dob;
    @JsonIgnore
    private String roles;

    public User()
    {
        super();
    }

    public User(String firstname, String lastname, String email, String username, String password, java.sql.Date dob, String roles) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.username = username;
        setPassword(password);
        this.dob = dob;
        this.roles = roles;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password=PASSWORD_ENCODER.encode(password);
    }

    public java.sql.Date getDob() {
        return dob;
    }

    public void setDob(java.sql.Date dob) {
        this.dob = dob;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
