package com.yubraj.user;

import com.yubraj.DTO.Userdto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@org.springframework.stereotype.Controller
public class UserController {

    @Autowired
    private UserRepository userrepo;

    @RequestMapping(value = "/login")
    public String login()
    {
        return "login";
    }

    @RequestMapping(value = "/register")
    public ModelAndView register()
    {
        ModelAndView mav=new ModelAndView();
        mav.setViewName("register");
        Userdto user=new Userdto();
        mav.addObject("user",user);

        return mav;
    }

    @RequestMapping(method = RequestMethod.POST,value = "/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid Userdto accountDto,
            BindingResult result, WebRequest request, Errors errors) {
        System.out.println("hello");
        User registered = new User();
        if (!result.hasErrors()) {
            registered = createUserAccount(accountDto, result);
        }
        if (registered == null) {
            result.rejectValue("email","Account Already Exists");
        }
        if (result.hasErrors()) {
            return new ModelAndView("register", "user", accountDto);
        }
        else {
            return new ModelAndView("successRegister","user", accountDto);
        }
    }
    private User createUserAccount(Userdto accountDto, BindingResult result) {
        User registered = null;
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            registered = new User(
                    accountDto.getFirstname(),
                    accountDto.getLastname(),
                    accountDto.getEmail(),
                    accountDto.getUsername(),
                    accountDto.getPassword(),
                    sqlDate,
                    "USER"
            );

            if(userrepo.findByEmail(registered.getEmail())!=null ||
                    userrepo.findByUsername(registered.getUsername())!=null)
                return null;

            registered = userrepo.save(registered);
        } catch (Exception e) {
            return null;
        }
        return registered;
    }

    @RequestMapping(value = "/app/error")
    public String error()
    {
        return "403";
    }
}
