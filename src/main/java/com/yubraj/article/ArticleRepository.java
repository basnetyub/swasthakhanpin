package com.yubraj.article;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by yubraj on 6/17/17.
 */
public interface ArticleRepository extends PagingAndSortingRepository<Article,Long> {
}
