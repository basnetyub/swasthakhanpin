package com.yubraj.article;
import com.yubraj.core.BaseEntity;
import com.yubraj.user.User;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Date;

/**
 * Created by yubraj on 6/17/17.
 */
@Table(name = "article")
@Entity
public class Article extends BaseEntity {
    private String title;
    private String description;
    private java.sql.Date publishdate;
    @OneToOne
    private User user;

    protected Article(){
        super();
    }

    public Article(String title, String description, Date publishdate, User user) {
        this.title = title;
        this.description = description;
        this.publishdate = publishdate;
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
