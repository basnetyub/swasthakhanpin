package com.yubraj.article;

import com.yubraj.DTO.Articledto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.xml.transform.Result;

/**
 * Created by black_flame on 7/26/2017.
 */


@Controller
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @RequestMapping("/submitArticle")
    public ModelAndView submit(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("submitArticle");
        Articledto article = new Articledto();
        modelAndView.addObject("article",article);
        return modelAndView;
    }


    @RequestMapping(value = "/submitArticle", method = RequestMethod.POST)
    public ModelAndView submitArticle(@ModelAttribute("article") @Valid Articledto articledto){

        Article newArticle = new Article();
        createArticle(articledto);
//        if(!result.hasErrors()){
//            newArticle=createArticle(articledto);
//        }
//        if(newArticle == null){
//            result.rejectValue("Title","Title Already Exists");
//
//        }
//        if(result.hasErrors()){
//            return new ModelAndView("submitArticle","article", articledto);
//        }
//        else{
//            return new ModelAndView("sucessArticle","article", articledto);
//        }
        return new ModelAndView("sucessArticle","article",articledto);

    }

    private Article createArticle(Articledto articledto){
        Article newArticle = null;
        try{
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            newArticle = new Article(
                    articledto.getTitle(),
                    articledto.getDescription(),
                    sqlDate,
                    articledto.getUser()
            );

            articleRepository.save(newArticle);


        }
        catch (Exception e){
            e.printStackTrace();
        }
        return newArticle;
    }

}
