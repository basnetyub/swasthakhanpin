package com.yubraj.core;

import com.yubraj.user.User;
import com.yubraj.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by yubraj on 6/12/17.
 */
@Component
public class DataBaseLoader implements ApplicationRunner {
    private final UserRepository users;

    @Autowired
    public DataBaseLoader(UserRepository users) {
        this.users = users;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        java.util.Date utilDate = new java.util.Date();
//        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
//        List<User> students= Arrays.asList(
//               new User("admin","admin","admin","admin","admin",sqlDate,"USER"),
//               new User("yub","yub","yub","yub","yub",sqlDate,"ADMIN")
//        );
//        users.save(students);


    }
}
