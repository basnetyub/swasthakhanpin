package com.yubraj.core;

import javax.persistence.*;

/**
 * Created by yubraj on 6/17/17.
 */
//class can be mapped as the entity class
@MappedSuperclass
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private final Long id;

    //for versioning the api. Etags header created
    @Version
    private Long version;
    protected BaseEntity() {
        id = null;
    }
}
